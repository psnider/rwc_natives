# Trees
## Big Leaf Maple
nice shade, colors
![Big Leaf Maple](http://www.laspilitas.com/images/grid24_24/1358/s/images/plants/1095/Acer_macrophyllum-2.jpg)  

[Details](http://www.laspilitas.com/nature-of-california/plants/acer-macrophyllum)

## Madrone
![Madrone](http://www.laspilitas.com/images/grid24_24/4851/s/images/plants/35/Arbutus_menziesii-5.jpg)

[Details](http://www.laspilitas.com/nature-of-california/plants/arbutus-menziesii)

# Shrubs
## Hoovers Manzanita
![Hoovers Manzanita](http://www.laspilitas.com/images/grid24_24/2917/s/images/plants/53/Arctostaphylos_hooveri_Hoovers_Manzanita.jpg)  
![Hoovers Manzanita](http://www.laspilitas.com/images/grid24_24/11153/images/plants/arctostaphylos/arctostaphylos-hooveri-hoovers-manzanita.jpg)

[Details](http://www.laspilitas.com/nature-of-california/plants/arctostaphylos-hooveri-hoovers-manzanita)

## Blackfruit Dogwood
![Blackfruit Dogwood](http://www.laspilitas.com/images/grid24_24/9569/images/plants/cornus/cornus-sessilis-leaves.jpg)  

[Details](http://www.laspilitas.com/nature-of-california/plants/cornus-sessilis)

# Flowering Shrubs
## Yarrow
brings butterflies

### (white) Yarrow
![White Yarrow](http://www.laspilitas.com/images/grid24_24/8544/s/images/plants/14/Achillea_millefolium_californica.jpg)  

[Details](http://www.laspilitas.com/nature-of-california/plants/achillea-millefolium-californica)

### Pink Yarrow
![Pink Yarrow](http://www.laspilitas.com/images/grid24_12/9198/s/images/plants/712/Achillea_millefolium_rosea_Island_Pink.jpg)  

[Details](http://www.laspilitas.com/nature-of-california/plants/achillea-millefolium-rosea-island-pink)

## Lilacs  
### California Lilacs
![California Lilacs](http://www.laspilitas.com/images/grid24_18/1112/s/images/plants/183/Ceanothus_Concha-1.jpg)  

[Details](http://www.laspilitas.com/groups/ceanothus/california_ceanothus.html)

### Ceanothus parryi
![Ceanothus parryi](http://www.laspilitas.com/images/grid24_12/946/s/images/plants/166/Ceanothus_parryi.jpg)  

[Details](http://www.laspilitas.com/nature-of-california/plants/ceanothus-parryi)

### Big Sur Lilac
![Big Sur Lilac](http://www.laspilitas.com/images/grid24_12/6040/s/images/plants/174/Ceanothus_thyrsiflorus_Big_Sur_California_lilac.jpg)  

[Details](http://www.laspilitas.com/nature-of-california/plants/ceanothus-thyrsiflorus-big-sur-california-lilac)

### Blue Mountain Lilac
![Blue Mountain Lilac](http://www.laspilitas.com/images/grid24_24/10066/images/plants/ceanothus/ceanothus-skylark.jpg)  

[Details](http://www.laspilitas.com/nature-of-california/plants/ceanothus-thyrsiflorus-skylark)

# Flowers and fillers
## Dwarf Coastal Manzanita
![Dwarf Coastal Manzanita](http://www.laspilitas.com/images/grid24_12/1589/s/images/plants/711/Arctostaphylos_edmundsii_Big_Sur_Manzanita.jpg)
  
[Details](http://www.laspilitas.com/nature-of-california/plants/arctostaphylos-edmundsii-big-sur-manzanita)

## Wild Ginger  

![Wild Ginger](http://1.bp.blogspot.com/_tL-ws3Vf4TA/TAKl6Wc_ULI/AAAAAAAAkPs/AbX4B2hDyos/s1600/IMG_5358.JPG)
[Details](http://www.laspilitas.com/nature-of-california/plants/asarum-caudatum)

## Marsh Baccharis and Douglas Baccharis  
![Marsh Baccharis](http://www.laspilitas.com/images/grid24_24/7558/s/images/plants/110/Baccharis_douglasii.jpg)

[Details](http://www.laspilitas.com/nature-of-california/plants/baccharis-douglasii)


## Pacific Bleeding Heart 
![Pacific Bleeding Heart](http://www.nwplants.com/images/commons/Dicentra_formosa_formosa_jen0424041.jpg)  

[Details](http://www.laspilitas.com/nature-of-california/plants/dicentra-formosa)

## Fireweed  
![Fireweed](http://imavex.vo.llnwd.net/o18/clients/urbanfarm/images/Perennial/Epilobium_Angustifolium_Seeds.JPG)

[Details](http://www.laspilitas.com/nature-of-california/plants/epilobium-angustifolium)

## Yerba Santa
![Yerba Santa](http://www.laspilitas.com/images/grid24_24/10057/images/plants/eriodictyon/eriodictyon-californicum.jpg)


[Details](http://www.laspilitas.com/nature-of-california/plants/eriodictyon-californicum)

## California Honeysuckle  
![California Honeysuckle](http://www.laspilitas.com/images/grid24_24/7193/s/images/plants/398/Lonicera_hispidula-1.jpg)

[Details](http://www.laspilitas.com/nature-of-california/plants/lonicera-hispidula)

# Grasses
## Thurber's Bentgrass  
![Thurber's Bentgrass](http://www.laspilitas.com/images/grid24_24/1015/s/images/plants/12/Agrostis_thurberiana-1.jpg)

[Details](http://www.laspilitas.com/nature-of-california/plants/agrostis-thurberiana)

## Globe Sedge  
![Globe Sedge](http://www.laspilitas.com/images/grid24_12/160/s/images/plants/732/Carex_globosa.jpg)

[Details](http://www.laspilitas.com/nature-of-california/plants/carex-globosa)
